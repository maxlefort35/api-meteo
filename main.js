function getHU() {
  document.getElementById("ville").innerHTML = document.getElementById("hu").value;
}

var callBackGetSuccess = function(data) {
  var element = document.getElementById("zone_meteo");
  element.innerHTML = "La temperature aujourd'hui est de " + data.list[0].main.temp + "C°";
}

function get_meteo() {
    var url = "https://api.openweathermap.org/data/2.5/forecast?q="+document.getElementById("ville").innerHTML+",fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

    $.get(url, callBackGetSuccess).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}

var callBackGetSuccess1 = function(data) {
  var element = document.getElementById("zone_prev");
  element.innerHTML = "La previson de demain est de " + data.list[4].main.temp + "C°";
}

function get_prevision() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?q=Paris,fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

  $.get(url, callBackGetSuccess1).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}

var callBackGetSuccess2 = function(data) {
  var element = document.getElementById("zone_prev2");
  element.innerHTML = "La previson dans 2 jours est de " + data.list[12].main.temp + "C°";
}

function get_prevision2() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?q=Paris,fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

  $.get(url, callBackGetSuccess2).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}

var callBackGetSuccess3 = function(data) {
  var element = document.getElementById("zone_prev3");
  element.innerHTML = "La previson dans 3 jours est de " + data.list[20].main.temp + "C°";
}

function get_prevision3() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?q=Paris,fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

  $.get(url, callBackGetSuccess3).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}

var callBackGetSuccess4 = function(data) {
  var element = document.getElementById("zone_prev4");
  element.innerHTML = "La previson dans 4 jours est de " + data.list[28].main.temp + "C°";
}

function get_prevision4() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?q=Paris,fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

  $.get(url, callBackGetSuccess4).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}

var callBackGetSuccess5 = function(data) {
  var element = document.getElementById("zone_prev5");
  element.innerHTML = "La previson dans 5 jours est de " + data.list[36].main.temp + "C°";
}

function get_prevision5() {
  var url = "https://api.openweathermap.org/data/2.5/forecast?q=Paris,fr&appid=5993ddb21fe96e2c75cb7351b5bea1ba&units=metric"

  $.get(url, callBackGetSuccess5).done(function() {
      //alert( "second success" );
    })
    .fail(function() {
    alert( "error" );
    })
    .always(function() {
      //alert( "finished" );
    });
}